export interface Habits {
  id: number;
  name: string;
  description: string;
  createdAt: string | null;
  updatedAt: string | null;
  deletedAt: string | null;
  checked: string;
  userId: number;
}
