export interface Roles {
  id: number;
  name: string;
}

export interface PersonalHabits {
  id: number;
  name: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}

export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
  roles: Roles[];
  personalHabits: PersonalHabits[];
}

export interface AuthResponse {
  user: User;
  token: string;
}
