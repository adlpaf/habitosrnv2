export interface HistoricalHabits {
  id: number;
  user_id: number;
  personal_habit_id: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}
