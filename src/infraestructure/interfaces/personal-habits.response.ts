export interface PersonalHabits {
  id: number;
  name: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
  checked: string;
  userId: number;
}
