import {Habits} from '../../domain/entities/habits';
import {PersonalHabits} from '../interfaces/personal-habits.response';

export class HabitsMapper {
  static personalHabitsToEntity(personalHabits: PersonalHabits): Habits {
    console.log(personalHabits);

    return {
      id: personalHabits.id,
      name: personalHabits.name,
      description: personalHabits.description,
      createdAt: personalHabits.createdAt,
      updatedAt: personalHabits.updatedAt,
      deletedAt: personalHabits.deletedAt,
      checked: personalHabits.checked,
      userId: personalHabits.userId,
    };
  }
}
