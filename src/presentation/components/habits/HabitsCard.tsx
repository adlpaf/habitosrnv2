import React from 'react';
import {Habits} from '../../../domain/entities/habits';
import {Card, Text, Toggle} from '@ui-kitten/components';
import {NavigationProp, useNavigation} from '@react-navigation/native';
import {RootStackParams} from '../../navigation/StackNavigator';
import {setHabitByUserAndPersonalHabit} from '../../../actions/habits/set-habit-by-user-and-personal-habit';

interface Props {
  habit: Habits;
}

export const HabitsCard = ({habit}: Props) => {
  console.log(habit);

  const [activeChecked, setActiveChecked] = React.useState(
    habit.checked === 'true',
  );
  const AddPersonalHabitChecked = async (isChecked: boolean): Promise<void> => {
    await setHabitByUserAndPersonalHabit(habit.id);

    setActiveChecked(isChecked);
  };

  const navigation = useNavigation<NavigationProp<RootStackParams>>();

  return (
    <Card
      style={{
        flex: 1,
        backgroundColor: '#F9F9F9',
        margin: 3,
        alignSelf: 'flex-start',
        flexDirection: 'row',
      }}
      onPress={() =>
        navigation.navigate('HabitsScreen', {habitId: habit.id.toString()})
      }>
      <Toggle
        checked={activeChecked}
        disabled={activeChecked ? true : false}
        onChange={AddPersonalHabitChecked}>
        <Text>
          {habit.id} - {habit.name}
        </Text>
      </Toggle>
    </Card>
  );
};
