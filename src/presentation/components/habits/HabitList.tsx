import React, {useState} from 'react';
import {Habits} from '../../../domain/entities/habits';
import {Button, Layout, List} from '@ui-kitten/components';
import {HabitsCard} from './HabitsCard';
import {RefreshControl} from 'react-native';
import {MyIcon} from '../ui/MyIcon';
import {useAuthStore} from '../../store/auth/useAuthStore';

interface Props {
  habits: Habits[];
}

export const HabitList = ({habits}: Props) => {
  const {logout} = useAuthStore();
  const [isRefresh, setIsRefresh] = useState(false);
  const onPulToRefresh = async () => {
    setIsRefresh(true);
    await new Promise(resolve => setTimeout(resolve, 2000));
    setIsRefresh(false);
  };
  return (
    <Layout>
      <List
        data={habits}
        numColumns={1}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => <HabitsCard habit={item} />}
        ListFooterComponent={() => <Layout style={{height: 150}} />}
        refreshControl={
          <RefreshControl refreshing={isRefresh} onRefresh={onPulToRefresh} />
        }
      />
      <Button
        accessoryRight={<MyIcon name="arrow-forward-outline" white />}
        onPress={logout}>
        Salir
      </Button>
    </Layout>
  );
};
