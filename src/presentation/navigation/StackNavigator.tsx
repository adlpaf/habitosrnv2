import React from 'react';

import {
  StackCardStyleInterpolator,
  createStackNavigator,
} from '@react-navigation/stack';
import {RegisterScreen} from '../screens/auth/RegisterScreen';
import {LoginScreen} from '../screens/auth/LoginScreen';
import {LoadingScreen} from '../screens/loading/LoadingScreen';
import {HabitsScreen} from '../screens/habits/HabitsScreen';
import {HomeScreen} from '../screens/home/HomeScreen';

export type RootStackParams = {
  LoadingScreen: undefined;
  LoginScreen: undefined;
  RegisterScreen: undefined;
  HomeScreen: undefined;
  HabitsScreen: {habitId: string};
};

const Stack = createStackNavigator<RootStackParams>();

const fadeAnimation: StackCardStyleInterpolator = ({current}) => {
  return {
    cardStyle: {
      opacity: current.progress,
    },
  };
};

export const StackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="LoadingScreen"
      screenOptions={{
        headerShown: false,
        // cardStyleInterpolator: fadeAnimation,
      }}>
      <Stack.Screen
        options={{cardStyleInterpolator: fadeAnimation}}
        name="LoadingScreen"
        component={LoadingScreen}
      />
      <Stack.Screen
        options={{cardStyleInterpolator: fadeAnimation}}
        name="LoginScreen"
        component={LoginScreen}
      />
      <Stack.Screen
        options={{cardStyleInterpolator: fadeAnimation}}
        name="RegisterScreen"
        component={RegisterScreen}
      />
      <Stack.Screen
        options={{cardStyleInterpolator: fadeAnimation}}
        name="HomeScreen"
        component={HomeScreen}
      />
      <Stack.Screen name="HabitsScreen" component={HabitsScreen} />
    </Stack.Navigator>
  );
};
