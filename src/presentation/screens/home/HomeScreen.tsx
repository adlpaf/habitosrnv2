import React from 'react';
// import {Button, Icon, Layout, Text} from '@ui-kitten/components';
// import {useAuthStore} from '../../store/auth/useAuthStore';
import {useQuery} from '@tanstack/react-query';
import {getHabitsByUser} from '../../../actions/habits/get-habits-by-user';
import {MainLayout} from '../../layouts/MainLayout';
import {FullScreenLoader} from '../../components/ui/FullScreenLoader';
import {HabitList} from '../../components/habits/HabitList';

export const HomeScreen = () => {
  const {isLoading, data: habits = []} = useQuery({
    queryKey: ['habits', 'infinite'],
    staleTime: 1000 * 60 * 60,
    queryFn: () => getHabitsByUser(),
  });
  console.log(habits);

  return (
    <MainLayout
      title="Personal Habits"
      subTitle="Aplicación de seguimiento de hábitos">
      {isLoading ? <FullScreenLoader /> : <HabitList habits={habits} />}
    </MainLayout>
  );
};
