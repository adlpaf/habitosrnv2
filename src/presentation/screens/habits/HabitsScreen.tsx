import React, {useRef} from 'react';
import {MainLayout} from '../../layouts/MainLayout';
import {Button, Input, Layout} from '@ui-kitten/components';
import {useQuery} from '@tanstack/react-query';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackParams} from '../../navigation/StackNavigator';
import {getHabitsById} from '../../../actions/habits/get-habit-by-id';
import {ScrollView} from 'react-native-gesture-handler';
import {MyIcon} from '../../components/ui/MyIcon';
import {Formik} from 'formik';

interface Props extends StackScreenProps<RootStackParams, 'HabitsScreen'> {}

export const HabitsScreen = ({route}: Props) => {
  const habitIdRef = useRef(route.params.habitId);
  const {data: habit} = useQuery({
    queryKey: ['habit', habitIdRef.current],
    queryFn: () => getHabitsById(habitIdRef.current),
  });

  if (!habit) {
    return <MainLayout title="Cargando..." />;
  }

  return (
    <Formik initialValues={habit} onSubmit={values => console.log(values)}>
      {({handleChange, handleSubmit, values, errors, setFieldValue}) => (
        <MainLayout title={values.name} subTitle={values.description}>
          <ScrollView style={{flex: 1}}>
            <Layout style={{marginHorizontal: 10}}>
              <Input
                label="ID"
                value={values.id.toString()}
                style={{marginVertical: 5}}
                disabled
              />
              <Input
                label="Nombre"
                value={values.name}
                style={{marginVertical: 5}}
              />
              <Input
                label="Descipción"
                value={values.description}
                style={{marginVertical: 5}}
                multiline
                numberOfLines={5}
                onChangeText={handleChange('description')}
              />
            </Layout>
            <Button
              onPress={() => console.log('Guardar')}
              style={{margin: 15}}
              accessoryLeft={<MyIcon name="save-outline" white />}>
              Guardar
            </Button>
            <Layout style={{height: 200}} />
          </ScrollView>
        </MainLayout>
      )}
    </Formik>
  );
};
