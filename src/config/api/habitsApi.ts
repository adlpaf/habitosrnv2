import {API_URL} from '@env';
import axios from 'axios';
import {StorageAdapter} from '../adapters/async-storage';

const habitsApi = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

habitsApi.interceptors.request.use(async config => {
  const token = await StorageAdapter.getItem('habitsToken');
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export {habitsApi};
