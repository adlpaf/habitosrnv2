import {habitsApi} from '../../config/api/habitsApi';
import type {
  AuthResponse,
  User,
} from '../../infraestructure/interfaces/auth.responses';

const returnUserToken = (data: AuthResponse) => {
  const user: User = {
    id: data.user.id,
    name: data.user.name,
    email: data.user.email,
    password: data.user.password,
    createdAt: data.user.createdAt,
    updatedAt: data.user.updatedAt,
    deletedAt: data.user.deletedAt,
    roles: data.user.roles,
    personalHabits: data.user.personalHabits,
  };

  return {
    user: user,
    token: data.token,
  };
};

export const authLogin = async (email: string, password: string) => {
  email = email.toLowerCase();
  try {
    const {data} = await habitsApi.post<AuthResponse>('/login', {
      correo: email,
      password,
    });
    console.log(data);
    return returnUserToken(data);
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const authCheckStatus = async () => {
  try {
    const {data} = await habitsApi.get<AuthResponse>('/check-status');

    return returnUserToken(data);
  } catch (error) {
    console.log({error});
    return null;
  }
};

export const authLogout = async () => {
  try {
    const {data} = await habitsApi.get<AuthResponse>('/logout');

    return returnUserToken(data);
  } catch (error) {
    console.log({error});
    return null;
  }
};
