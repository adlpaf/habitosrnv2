import {habitsApi} from '../../config/api/habitsApi';
import {HistoricalHabits} from '../../infraestructure/interfaces/historical-habits.response';

export const setHabitByUserAndPersonalHabit = async (id: number) => {
  try {
    const {data} = await habitsApi.post<HistoricalHabits>(
      '/v1/addHistoricalHabit',
      {
        id,
      },
    );
    // const habits = data.map(HabitsMapper.personalHabitsToEntity);
    console.log(data);

    return data;
  } catch (error) {
    console.log('Error: ' + error);
    throw new Error('Error quetting habits');
  }
};
