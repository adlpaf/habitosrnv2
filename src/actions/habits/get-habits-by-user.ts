import {habitsApi} from '../../config/api/habitsApi';
import {PersonalHabits} from '../../infraestructure/interfaces/personal-habits.response';
import {HabitsMapper} from '../../infraestructure/mappers/habits.mapper';

export const getHabitsByUser = async () => {
  try {
    const {data} = await habitsApi.get<PersonalHabits[]>('/v1/personalHabits');
    const habits = data.map(HabitsMapper.personalHabitsToEntity);
    console.log(data);

    return habits;
  } catch (error) {
    console.log('Error: ' + error);
    throw new Error('Error quetting habits');
  }
};
