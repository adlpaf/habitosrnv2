import {habitsApi} from '../../config/api/habitsApi';
import {Habits} from '../../domain/entities/habits';
import {PersonalHabits} from '../../infraestructure/interfaces/personal-habits.response';
// import {Habits as IHabits} from '../../infraestructure/interfaces/habits.response';
import {HabitsMapper} from '../../infraestructure/mappers/habits.mapper';

export const getHabitsById = async (id: string): Promise<Habits> => {
  try {
    const {data} = await habitsApi.get<PersonalHabits>(
      `/v1/personalHabit/${id}`,
    );
    return HabitsMapper.personalHabitsToEntity(data);
  } catch (error) {
    console.log(error);
    throw new Error(`Error getting habit by id: ${id}`);
  }
};
