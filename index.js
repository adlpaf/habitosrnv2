/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import {HabitsApp} from './src/HabitsApp';

AppRegistry.registerComponent(appName, () => HabitsApp);
